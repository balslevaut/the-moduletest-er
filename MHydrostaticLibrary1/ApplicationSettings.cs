﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO.Ports;


namespace MHydrostaticLibrary1
{
    public enum LogSeverity
    {
        SystemError,
        Alarm
    }

    public class ApplicationSettingsM
    {



        public settingsData AppSettingsData = new settingsData();


        public struct settingsData
        {
            public string AppFolderPath;
            public string DataFolderPath;
            public int HourForReading;

            public bool UseSubSampling; //Sample more often than writing to the log file . purpose is to even out waves influence on the logged data
            public int SampleRate; //seconds between reading the instrument should be less than the logRate
            public int DataLogRate; //the Seconds between writing to the logfile

            public float LevelOffset; //Correction for the level measure 
            
            public string ComportName;
            public int Baudrate;
            public Parity parity;
            public int Databits;
            public StopBits stopbits;

        }




        public void ReadAllsettings()
        {
            try
            {
                string Filepath = @"MHydrostaticServer.exe.config";
                XmlDocument doc = new XmlDocument();
                doc.Load(Filepath);

                XmlNode node = doc.DocumentElement.SelectSingleNode("/configuration/appSettings/UseSubSampling");
                if (string.Compare(node.FirstChild.Value, "yes", StringComparison.CurrentCultureIgnoreCase) == 0)
                { AppSettingsData.UseSubSampling = true; }
                else { AppSettingsData.UseSubSampling = false; }

                node = doc.DocumentElement.SelectSingleNode("/configuration/appSettings/SampleRate");
                AppSettingsData.SampleRate = Convert.ToInt32(node.FirstChild.Value);
                node = doc.DocumentElement.SelectSingleNode("/configuration/appSettings/DataLogRate");
                AppSettingsData.DataLogRate = Convert.ToInt32(node.FirstChild.Value);

                node = doc.DocumentElement.SelectSingleNode("/configuration/appSettings/DataFolderPath");
                AppSettingsData.DataFolderPath = node.FirstChild.Value;

                node = doc.DocumentElement.SelectSingleNode("/configuration/appSettings/CLockForReading");
                bool succededQ = int.TryParse(node.FirstChild.Value, out AppSettingsData.HourForReading);


                XmlNode node3 = doc.DocumentElement.SelectSingleNode("/configuration/appSettings/PortSettings");

                AppSettingsData.ComportName = node3.ChildNodes[0].FirstChild.Value;
                AppSettingsData.Baudrate = Convert.ToInt32(node3.ChildNodes[1].FirstChild.Value);
                AppSettingsData.parity = ConvertToParity(node3.ChildNodes[2].FirstChild.Value);

                AppSettingsData.Databits = Convert.ToInt32(node3.ChildNodes[3].FirstChild.Value);
                AppSettingsData.stopbits = ConvertToStopbits(node3.ChildNodes[4].FirstChild.Value);

                var temp = node3.ChildNodes[2].FirstChild.Value;



            }
            catch (Exception ex)
            {

                //alarmn  here
            }



            //XmlNode node = doc.DocumentElement.SelectSingleNode("/mineting/Path");
            //var temp = node.FirstChild.Value;
        }

        /// <summary>
        /// Typecast of parity string to Parity type
        /// </summary>
        /// <param name="strvalue">The parity as string</param>
        /// <returns></returns>
        private Parity ConvertToParity(string strvalue)
        {
            Parity theresult = new Parity();

            if (string.Compare(strvalue, "None", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = Parity.None;
            }
            if (string.Compare(strvalue, "Odd", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = Parity.Odd;
            }
            if (string.Compare(strvalue, "Even", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = Parity.Even;
            }
            if (string.Compare(strvalue, "Mark", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = Parity.Mark;
            }
            if (string.Compare(strvalue, "Space", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = Parity.Space;
            }


            return theresult;
        }

        /// <summary>
        /// Typecast of Stopbits to stopbits type
        /// </summary>
        /// <param name="strvalue">The stopbits as string</param>
        /// <returns></returns>
        private StopBits ConvertToStopbits(string strvalue)
        {
            StopBits theresult = new StopBits();

            if (string.Compare(strvalue, "0", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = StopBits.None;
            }
            if (string.Compare(strvalue, "1", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = StopBits.One;
            }
            if (string.Compare(strvalue, "2", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = StopBits.Two;
            }
            if (string.Compare(strvalue, "1,5", StringComparison.CurrentCultureIgnoreCase) == 0 || string.Compare(strvalue, "1.5", StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                theresult = StopBits.OnePointFive;
            }
            return theresult;
        }

    }
}
