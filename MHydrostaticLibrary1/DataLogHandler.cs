﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MHydrostaticLibrary1
{
    class DataLogHandler
    {
        public string DataLogPath;
        public DateTime whentostartnewfile;

        /// <summary>
        /// Write the dataset to the data resultfile
        /// </summary>
        /// <param name="Headertext">text for identifying the sensor , some relevamnt settings etc.</param>
        /// <param name="Depth"></param>
        /// <param name="Temperature"></param>
        /// <param name="UtcTimestamp"></param>
        public void LogDataset(string Headertext, float Depth, float Temperature, DateTime UtcTimestamp)
        {
            try
            {
                string FileName = GetDataLogFileNameAndPath();


                //create the folder if it doesnt exist
                string mypath = GetFolderPath();
                if (!Directory.Exists(mypath))
                {
                    Directory.CreateDirectory(mypath);
                }

                Encoding encoding = Encoding.GetEncoding("ISO-8859-1"); // Encoding for the csv file
                FileStream fs;//= new FileStream(FileName, FileMode.CreateNew);
                StreamWriter writer;// = new StreamWriter(fs, encoding);
                StringBuilder sb = new StringBuilder();
                //Create the file If it doesnt exist 
                if (!File.Exists(FileName))
                {
                    #region legend
                    // create the file 
                    fs = new FileStream(FileName, FileMode.CreateNew);
                    writer = new StreamWriter(fs, encoding);
                    // and write the header.
                    sb.Clear();
                    sb.Append(Headertext);
                    string dummystring = sb.ToString();
                    writer.WriteLine(dummystring);
                    //writer.Flush();

                    //write the legend
                    sb.Clear();
                    sb.Append("dataTime" + ",");
                    sb.Append("Sealevel" + ",");
                    sb.Append("Temperature" + ",");
                    sb.Append("dateTime");

                    dummystring = sb.ToString();
                    writer.WriteLine(dummystring);
                    writer.Flush();
                    fs.Close();
                    #endregion Legend
                }
                else
                { }
                //var outFile = File.CreateText(filename); 
                //outFile.WriteLine(sb.ToString());
                #region Write the data to file;
                {
                    fs = new FileStream(FileName, FileMode.Append);
                    writer = new StreamWriter(fs, encoding);
                    sb.Clear();
                    sb.Append(UtcTimestamp.ToString("yyyy_MM_dd hh:mm:ss") + "," + Depth.ToString("N2") + "," + Temperature.ToString("N2"));
                    writer.WriteLine(sb.ToString());
                    writer.Flush();
                    fs.Close();
                }
                #endregion Write the data to file;
                //outFile.Close();

            }
            catch (Exception ex)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, "DataLogHandler: logdataset", ex.Message);
            }

        }//EndOfMethod



        /// <summary>
        /// returning the complete filename for the datalogfile 
        /// </summary>
        /// <returns></returns>
        private string GetDataLogFileNameAndPath()
        {
            string fileEventName = null;

            int hourForReading = GlobalM.applicationSettingsM.AppSettingsData.HourForReading;
            DateTime dt = DateTime.UtcNow;

            if (dt.Hour < hourForReading)
            {
                // We use the data from previous day                  
                fileEventName = GetFolderPath() + "Data_Log_" + DateTime.UtcNow.AddHours(-24).ToString("yyyy_MMM_dd") + ".csv";
            }
            else
            {
                fileEventName = GetFolderPath() + "Data_Log_" + DateTime.UtcNow.ToString("yyyy_MMM_dd") + ".csv";
            }

            return fileEventName;
        }//EndOfMethod

        private string GetFolderPath()
        {
            return GlobalM.applicationSettingsM.AppSettingsData.DataFolderPath + @"\";
        }//EndOfMethod

    }
}
