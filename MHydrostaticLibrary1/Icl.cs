﻿using MCommonLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace MIclServerLibrary
{
    // Return 'true' and related response in case of success
    // Return 'false' in case of error and the 'loggerString' might instead contains some kind of error description

    class Icl : IDisposable
    {
        SerialPort _serialPort;

        // Is used to prevent the thread from dying
        private ManualResetEvent mre = new ManualResetEvent(false);

      

        // The portName 'comxx' where the ICL is connected
        string portName = null;

        private System.Threading.Timer hearAnythingFromIclIntervalTimer; //Timer that keeps an eye on that we hear anything from the ICL.
        string wantedMsgToBeSent = null;   // The commando string sent to the ICL

        enum IclState
        {
            NONE,                       // No activity, Just started
            ASK_FOR_FIRST_TIMESTAMP_AND_STATUS,    // We have asked for the timestamp for the first logger line
            ASK_FOR_DATA,               // We ask for data one or several times.
            SET_TIME,                   // Now we set the time
            DONE                        // Everything done
        }
        IclState iclState = IclState.NONE;

        // Contains received data just received from the RS232 interface
        StringBuilder tStringBuilder = new StringBuilder();
        int tStringAddCounter = 0;  // Counts each time the tString is updated
        private Mutex mut = new Mutex();   // Is used to protect the tString

        // Contains the totally build mld file that might contains input from several reads
        StringBuilder mldFileBuilder = new StringBuilder();

        int askForData = 0;

        // Contains data from current READ_U. So this is a total build for the received data from tString. This is neccessary because sometimes there is more than 3 seconds break in the datastream
        StringBuilder readUResponseBuilder = new StringBuilder();

        System.Timers.Timer myTimer = null;     // 3 seconds timer so we can keep an eye on whether no more lines are received from the logger.
                                                // As noticed above it sometimes happended that more data is received also after 3 seconds

        static int maxIclResponseTime = 120;    // 2 minutes.
        static TimeSpan tsInterval = new TimeSpan(0, 0, maxIclResponseTime);
        int iclMissingResponseCounter = 0;

        public void StopMe()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "StopMe");
            try
            {

                if (_serialPort != null)
                {
                    if (_serialPort.IsOpen == true)
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "StopMe: serial port was open and will be closed now");
                        _serialPort.Close();
                        _serialPort.Dispose();
                        _serialPort = null;
                    }
                    else
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "StopMe: serial port was not open");
                    }
                }

                if (hearAnythingFromIclIntervalTimer != null)
                {
                    hearAnythingFromIclIntervalTimer.Dispose();
                    hearAnythingFromIclIntervalTimer = null;
                }

                if (mre != null)
                {
                    mre.Dispose();
                    mre = null;
                }

                if (myTimer != null)
                {
                    myTimer.Dispose();
                    myTimer = null;
                }
            }
            catch (Exception ee)
            {
                EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "StopMe failed with ee= " + ee);
            }

        }


        // In case the ICl hasn't responded
        private void MyIclResponseTimer_elapsed(object state)
        {
            iclMissingResponseCounter++;
            EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "In MyIclResponseTimer_elapsed, iclMissingResponseCounter = " + iclMissingResponseCounter);

            if (iclMissingResponseCounter == 3)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "ICL never answered the request: '" + wantedMsgToBeSent + "'");
                // Return data via callback                      
                mre.Set();  // 'kill' the process            
            }
            else
            {
                // Try to send data again
                SendData(wantedMsgToBeSent, false);
            }
        }


        private void RestartICLResponseTime()
        {
            iclMissingResponseCounter = 0;

            EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "In RestartICLResponseTime");

            if (hearAnythingFromIclIntervalTimer != null)
            {
                hearAnythingFromIclIntervalTimer.Dispose();
                hearAnythingFromIclIntervalTimer = null;
            }
          
            hearAnythingFromIclIntervalTimer = new System.Threading.Timer(new
                 TimerCallback(MyIclResponseTimer_elapsed), null, (int)tsInterval.TotalMilliseconds,
                 (int)tsInterval.TotalMilliseconds);
        }

        public Icl(string portName)
        {
            this.portName = portName;
        }

        void SetTheTime()
        {
            // Now set the time
            DateTime dt = DateTime.UtcNow;
            string clockStr = "clock " + dt.Day + "," + dt.Month + "," + dt.Year + "," + dt.Hour + "," + dt.Minute + "," + dt.Second + "\n\r";
            EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "Change state to .SET_TIME");
            iclState = IclState.SET_TIME;
            Thread.Sleep(3000);  // WE wait 3 seconds so we are sure that data is not mixed
            SendData(clockStr, true);
        }


        void ExtractDataAndAskForTime(Mld mld)
        {

            string mldString = mld.GetMldString();

            if (mldString != null)
            {
                EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "ExtractDataAndAskForTime: " + mldString );
            }
            else
            {
                EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "ExtractDataAndAskForTime: 'No mld String'");
            }
            // Everything done
            Acd acd = null;

            if (mld.ProbeIsActive(0) == false && mld.ProbeIsActive(1) == false)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Serious warning: ICL indicates that none probes are active, might be because power is weak");
            }


            for (int n = 0; n < 2; n++)
            {
                if (mld.ProbeIsActive(n) == true)
                {
                    acd = new Acd();
                    acd.Set(MIclServerLibrary.EventLogHandler.MyCallbackLog, mld, n);
                    string fileName;
                    string acdAsText = acd.GetAcdFile(out fileName);
                    EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "acd: name ='" + fileName + "'");
                    string path = EventLogHandler.GetFilePath();
                    string fileNameAndPath = path + "\\" + fileName;
                    //  EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "Jens: Lige for jeg skriver ACD til HD");
                    File.WriteAllText(fileNameAndPath, acdAsText);
                    //  EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "Jens: Lige efter jeg skriver ACD til HD");                                  
                }
                else
                {
                    EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "mld.ProbeIsActive(" + n + ") == false");
                }
            }

           SetTheTime();
        }

        private void FirstTimeStampReceived(string decodedString)
        {           
            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "TimeStamp received: " + decodedString);

            if (decodedString != null && decodedString.Length > 1 && decodedString.Length < 5 && decodedString.Contains("OK"))
            {
                // The "OK" is received without content from "mark" and "status" command, so we wait
                EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "ICL only 'OK' received when asking for time stamp. Ignores");
                return;
            }

            if (decodedString != null && decodedString.Length > 3 && decodedString.Contains("WAIT"))
            {
                // When the ICL indicates that we have to wait because measurements are ongoing               
                EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "ICL BUSY (Wait received, First Timestamp received state) ignores)");                
                return;
            }

            try
            {
                StringBuilder sb = new StringBuilder();

                // Remove unwanted characters
                foreach (char c in decodedString)
                {
                    if (c == '\n' || c == '\r')
                    {
                        sb.Append(',');
                    }
                    else
                    {
                        sb.Append(c);
                    }
                }

                string[] splitString = new string[] { "," };
                // Get timeStamp that can be used as mark

                string[] parameters = sb.ToString().Split(splitString, StringSplitOptions.RemoveEmptyEntries);

                if (parameters.Length > 0 && parameters[0].Contains("OK"))
                {
                    // Remove OK
                    List<String> myList = parameters.ToList();
                    myList.RemoveAt(0);
                    parameters = myList.ToArray();

                }
                
                if (parameters.Length == 13)
                {
                    // We expect the length is '13' corresponding to the return information from "Mark" (5 parameters) and status (8 parameters)
                    if (Int32.Parse(parameters[6]) <= 1)
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Status command returns " + Int32.Parse(parameters[6]) + ", so no new data is requested");
                        SetTheTime();                       
                    }
                    else
                    {
                        string timeStamp = null;
                        if (parameters[4] == "0" && parameters[3] == "0" && parameters[2] == "0" && parameters[1] == "0" && parameters[0] == "0")
                        {
                            // Due to errors in the ICL returning zeros from the mark command migth indicate to things:
                            // 1) Etiher a new ICL where no data has been read yet
                            // 2) A full ICL with no new data
                            // We assume it is the first option here because we already have used the "status" command to ask about how many measurements are left in the logger
                            // Set the timestamp to something in the past
                            timeStamp = "27,4,2001,8,40";
                        }
                        else
                        {
                            // Assume we receive: OK,<day-of-month>,<month>,<year>,<hour>,<minute>,<second>
                            DateTime result = new DateTime(Int32.Parse(parameters[2]), Int32.Parse(parameters[1]), Int32.Parse(parameters[0]),
                                        Int32.Parse(parameters[3]), Int32.Parse(parameters[4]), 0);

                            // Create time stamp
                            timeStamp = result.Day.ToString() + "," + result.Month.ToString() + "," + result.Year.ToString() + "," + result.Hour.ToString() +
                            "," + result.Minute.ToString();

                        }

                        string nextCommand = "@OK;TYPE;VERSION;VERSION_DATE;SETUP_LOGPROBE;SETUP_PROBE 1;SETUP_PROBE 2;MARK "
                            + timeStamp + ";READ 25;MARK " + timeStamp + ";STATUS\n\r";

                        Thread.Sleep(3000);  // We wait 3 seconds so we are sure that data is not mixed
                        EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Ask for first data.");
                        iclState = IclState.ASK_FOR_DATA;                    
                        SendData(nextCommand, true);        
                    }
                }
                else
                {
                    //Something went wrong when we decoded the string. Maybe it is because that only "OK" has been received without timestamp that migth occur in moment     
                    //So we resend request. It will happend 2 minutes from now when timer times out.
                   // skywave.Status = Helper.ErrorTryAgain;
                    EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Error: Returned result from 'Mark;STATUS' command contains not the expected number of parameters (13). Instead is received: "  
                                + parameters.Length + ". Resend\n");
               
                    SendData("@OK;MARK;STATUS\n\r", true);
                    //SendData("@OK;STATUS\n\r", true);
                    iclState = IclState.ASK_FOR_FIRST_TIMESTAMP_AND_STATUS;
                    mldFileBuilder = new StringBuilder();

                }

            }
            catch (Exception ee)
            {
                 EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Something went wrong when decoding the first string that shold contain first time stamp:" + ee);                
            }

        }


        private void HandleReceivedloggerData(string str)
        {
            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "HandleReceivedloggerData, state = " + iclState);

            //  EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Jens, du er lige her");
            //  EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Jens, du er lige her 2");
            switch (iclState)
            {
                case IclState.ASK_FOR_FIRST_TIMESTAMP_AND_STATUS:                   
                    FirstTimeStampReceived(str);
                    break;

                case IclState.ASK_FOR_DATA:
                    try
                    {
                        askForData++; //Asked for data received one more time
                        // First check if all data is received from the given request
                        Mld mldReadUResponse = new Mld(EventLogHandler.MyCallbackLog);

                        //Add data to the read_u response string
                        readUResponseBuilder.Append(str);

                        string result = mldReadUResponse.SetStringFromRs232Channel(readUResponseBuilder);

                        if (result == null)
                        {                                                            
                            if (mldReadUResponse.GetNbOfMeasurementsLeft() < 2)
                            {
                                // Remove the header
                                mldReadUResponse.RemoveHeader(ref readUResponseBuilder);
                                mldFileBuilder.Append(readUResponseBuilder);

                                Mld mldTotal = new Mld(EventLogHandler.MyCallbackLog);
                                mldTotal.SetStringFromRs232Channel(mldFileBuilder);

                                // Extract data and ask for time
                                ExtractDataAndAskForTime(mldTotal);
                            }
                            else
                            {
                                /* Here we add the just received data and ask for more data */
                                string markDate = mldReadUResponse.GetMarkDate();

                                // Remove the header. Only if not first time
                                if(askForData >1)
                                {
                                    mldReadUResponse.RemoveHeader(ref readUResponseBuilder);  
                                }

                                // Remove the "footer" 
                                mldReadUResponse.RemoveLast2LinesIfStartWithOK(ref readUResponseBuilder);

                                mldFileBuilder.Append(readUResponseBuilder);
                                readUResponseBuilder.Clear();  // Prepare for new read_u

                                iclState = IclState.ASK_FOR_DATA;

                                string nextCommand = "@OK;TYPE;VERSION;VERSION_DATE;SETUP_LOGPROBE;SETUP_PROBE 1;SETUP_PROBE 2;MARK "
                                       + markDate + ";READ 50;MARK " + markDate + ";STATUS\n\r";

                                Thread.Sleep(3000);  // We wait 3 seconds so we are sure that data is not mixed
                                EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Ask for more data. Nb Of Data Left:" + mldReadUResponse.GetNbOfMeasurementsLeft());
                                SendData(nextCommand, true);
                            }
                           
                        }
                        else
                        {
                            // Here we wait on the rest of the data
                            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "All data not received. Received Err: " + result);
                        }
                    }
                    catch (Exception ee)
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Decoding MLD or ACD file failed: '" + ee.ToString() + "'");
                    }

                    break;

                case IclState.SET_TIME:

                    EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "IclState.SET_TIME");
                    if (!(str != null && str.Length > 2 && str.Contains("OK")))
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Failing to set the clock");
                    }
                    iclState = IclState.NONE;
                    // StopMe();

                    EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "Stop the thread");
                    mre.Set();
                    break;

                case IclState.NONE:
                    EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Received data before any command sent to logger");
                    break;

                case IclState.DONE:
                    EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Received data in state: IclState.DONE");
                    break;

                default:
                    EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Received data in unexpected state");
                    break;
            }
        }


        // Method that is fired if more than 3 seconds have passed since data last time was received. So it is expected that no more data is received
        void ElapsedReceivedLoggerDataHandler(object sender, ElapsedEventArgs e)
        {
            try
            {
                mut.WaitOne();
                string str = tStringBuilder.ToString();
                tStringBuilder.Clear();
                mut.ReleaseMutex();

                if (str.Length < 109600)
                //  if (str.Length < 80)
                {
                    EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "ElapsedReceivedLoggerDataHandler= '" + str + "'");
                }
                else
                {
                    // Shorten the received string to avoid getting a too big log file
                    EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "ElapsedReceivedLoggerDataHandler (Shortened)= '" + str.Substring(0, 79) + "'");
                }

                if (myTimer != null && myTimer.Enabled == true)
                {
                    myTimer.Stop(); // Stop the 3 seconds timer that keeps an eye on whether data is received from the ICL
                    myTimer.Dispose();
                    myTimer = null;
                }

                if (str.Length > 5 && str.Contains("WAITOK"))
                {
                    // Here we simply remove the "WAIT". WAIT and expected data received within 3 seconds
                    str = str.Substring(3);
                }

                if (str.Length > 3 && str.Contains("WAIT"))
                {
                    // Ignore
                    EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Wait received and ignores");
                    RestartICLResponseTime();  // Restart timer, so we are sure we are not blocked
                    return;
                }

                if (str.Length > 3 && (str.Contains("ERROR MEASUREMENT IN PROGRESS") || str.Contains("ERROR COM BUFFER OVERRUN")))
                {
                    EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Error code received: '" + str + "'");
                    RestartICLResponseTime();  // Restart timer, so we are sure we are not blocked
                    return;
                }
                else
                {
                }

                // Handle data
                HandleReceivedloggerData(str);
            }
            catch (Exception ee)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "ElapsedReceivedLoggerDataHandler failure: " + ee);
            }
        }

        public void _serialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Serial port Error received: '" + e + "'");

        }


        // Method that is invoked in case data is received from the logger
        public void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //  EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "_serialPort_DataReceived");  // Disabled because it generates a lot of traces

            if (myTimer != null)
            {
                myTimer.Stop(); // Stop the 3 seconds timer that fired the event
                myTimer.Dispose();
                myTimer = null;
            }

            if (hearAnythingFromIclIntervalTimer != null)
            {
                hearAnythingFromIclIntervalTimer.Dispose();
                hearAnythingFromIclIntervalTimer = null;
            }

            //Initialize a buffer to hold the received data 
            byte[] buffer = new byte[_serialPort.ReadBufferSize];

            // string _terminator = "\n\r";

            //There is no accurate method for checking how many bytes are read 
            //unless you check the return from the Read method 
            int bytesRead = _serialPort.Read(buffer, 0, buffer.Length);

            //For the example assume the data we are received is ASCII data. 

            string str = Encoding.ASCII.GetString(buffer, 0, bytesRead);

            // EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "_serialPort_DataReceived. Str: '" + str + "'");  // Disabled because it generates a lot of traces


            //  EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "str:" + str);  // Disabled because it generates a lot of traces

            mut.WaitOne();
            tStringBuilder.Append(str);
            mut.ReleaseMutex();

            tStringAddCounter++;

            if (tStringAddCounter % 100 == 0)
            {
                EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "_serialPort_DataReceived activity (100 times): " + str);  // Disabled because it generates a lot of traces
            }
                       
            // Start a 3  seconds timer so we can keep an eye on whether no more lines are received from the logger
            int t1 = 3 * 1000;
            myTimer = new System.Timers.Timer { Interval = t1 };
            myTimer.Enabled = true;
            myTimer.AutoReset = false;  //Timer should raise the Elapsed event only once 
            myTimer.Elapsed += (sender1, e1) => ElapsedReceivedLoggerDataHandler(sender1, e1);
        }

        // Remember to call this method in a try/catch
        public void StartMe()
        {
            try
            {
                EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "SerialPortInterface:StartMe(" + portName + ")");
                // Create a new SerialPort object with default settings.
                int counter = 2;
                Exception myExeption = null;

                while (counter > 0)
                {
                    try
                    {
                        _serialPort = new SerialPort();

                        // Allow the user to set the appropriate properties.
                        _serialPort.PortName = this.portName;
                        _serialPort.BaudRate = 9600;
                        _serialPort.Parity = Parity.None;
                        _serialPort.DataBits = 8;
                        _serialPort.StopBits = StopBits.One;

                        if (ConfigFileWrapper.ReadSetting("RtsEnable") == "true")
                        {
                            _serialPort.RtsEnable = true;

                            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "RtsEnable == 'true'");
                        }
                        else if (ConfigFileWrapper.ReadSetting("RtsEnable") == "false")
                        {
                            _serialPort.RtsEnable = false;
                            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "RtsEnable == 'false'");
                        }
                        else
                        {
                            throw new Exception("ERROR: App parameter: 'RtsEnable' not defined correct");
                        }


                        if (ConfigFileWrapper.ReadSetting("DtrEnable") == "true")
                        {
                            _serialPort.DtrEnable = true;

                            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "DtrEnable == 'true'");
                        }
                        else if (ConfigFileWrapper.ReadSetting("DtrEnable") == "false")
                        {
                            _serialPort.DtrEnable = false;
                            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "DtrEnable == 'false'");
                        }
                        else
                        {
                            throw new Exception("ERROR: App parameter: 'DtrEnable' not defined correct");
                        }
                        //_serialPort.Handshake = Handshake.None;

                        if (ConfigFileWrapper.ReadSetting("SerialPortHandshake") == "XOnXOff")
                        {
                            _serialPort.Handshake = Handshake.XOnXOff;
                            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "SerialPortHandshake == 'XOnXOff'");
                        }
                        else if (ConfigFileWrapper.ReadSetting("SerialPortHandshake") == "None")
                        {
                            _serialPort.Handshake = Handshake.None;
                            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "SerialPortHandshake == 'None'");
                        }
                        else
                        {
                            throw new Exception("ERROR: App parameter: 'SerialPortHandshake' not defined correct");
                        }
                        _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                        _serialPort.ErrorReceived += new SerialErrorReceivedEventHandler(_serialPort_ErrorReceived);

                        // Set the read/write timeouts
                        // _serialPort.ReadTimeout = 500;
                        //  _serialPort.WriteTimeout = 500;

                        _serialPort.Open();

                        counter = -1;
                    }
                    catch (System.IO.IOException ioexp)
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Com port System.IO.IOException: " + portName + " failed: " + ioexp);
                        Thread.Sleep(8000);
                        StopMe();
                        myExeption = ioexp;
                        Thread.Sleep(8000);
                        counter--;
                    }

                    catch (Exception ee)
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Attempt to open com port: " + portName + " failed: " + ee);
                        Thread.Sleep(8000);
                        StopMe();
                        myExeption = ee;
                        Thread.Sleep(8000);
                        counter--;
                    }
                }

                if (counter == 0)
                {
                    EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Serious error. Opening: '" + portName + "' has failed. Given up. Exception: " + myExeption);
                    return;
                }

                DateTime dt = DateTime.UtcNow + new TimeSpan(0,0,10);

                // Loop until worker thread activates. 
                while (!Thread.CurrentThread.IsAlive)
                {
                    if (DateTime.UtcNow > dt)
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Thread.CurrentThread.IsAlive took more than 10 seconds");
                        break;
                    }
                }

                // Wait on the thread. Without the 2 seconds break it has been seen that the request for data occurs before the rs232 communication is up and running correctly
                Thread.Sleep(2000);
              //  SendData("@OK;TYPE;VERSION;VERSION_DATE;SETUP_LOGPROBE;SETUP_PROBE 1;SETUP_PROBE 2;READ_U 50;STATUS\n\r", true); Jens
                    
                SendData("@OK;MARK;STATUS\n\r", true);
                //SendData("@OK;STATUS\n\r", true);
                iclState = IclState.ASK_FOR_FIRST_TIMESTAMP_AND_STATUS;
                mldFileBuilder = new StringBuilder();

                EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "Just before mre.WaitOne");
                mre.WaitOne();  // Prevent the thread from dying
                EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "Just after mre.WaitOne");

                StopMe();  // Kill everything
            }
            catch (ThreadAbortException abortException)
            {
                EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "Abort Exception received:" + abortException.ExceptionState);
                StopMe();  // Kill everything
            }
        }



        ~Icl()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "~Icl()");
            StopMe();
        }

        public void SendData(string message, bool restartCounterForFailingResponse)
        {
            try
            {
                wantedMsgToBeSent = message;
                EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "SendData = '" + message + "'");

                _serialPort.WriteLine(message);

                EventLogHandler.WriteEventLog(LogSeverity.Debug, portName, "SendData Done");

                // Start a timer that keeps an eye on that any kind of response is received from the logger.
                if (restartCounterForFailingResponse == true)
                {
                    RestartICLResponseTime();
                }
            }
            catch(Exception ee)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, portName, "Serious Error: SendData failed: " + ee);
            }
        }

        public void Dispose()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Info, portName, "ICL: Dispose called");
            StopMe();
        }
    }
}
