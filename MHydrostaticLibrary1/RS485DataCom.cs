﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.IO.Ports;


namespace MHydrostaticLibrary1
{
    public class RS485DataCom
    {


        #region Definitions
        int ActualSamplecount = 0;
        float AvgDepth = 0;
        float AvgTemperarture = 0;

        DataLogHandler theDataloghandler = new DataLogHandler();

        #region define CrcTable
        private static ushort[] CrcTable = {
        0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
        0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
        0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
        0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
        0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
        0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
        0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
        0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
        0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
        0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
        0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
        0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
        0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
        0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
        0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
        0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
        0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
        0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
        0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
        0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
        0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
        0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
        0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
        0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
        0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
        0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
        0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
        0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
        0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
        0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
        0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
        0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };
        #endregion define CrcTable

        public class Result
        {
            //this class is to hold a result record
            public float Depth;
            public float Temperature;
            public DateTime UtcTimestamp;

        }


        #endregion Definitions

        /// <summary>
        /// do the subsamlples so that the logged data is an average of the subsamples values
        /// </summary>
        /// <returns></returns>
        public bool DoSampleData()
        {// do the subsamlples so that the logged data is an average of the subsamples values
            bool result = false;

            Result Res = GetDataRS485();
            if (Res != null)
            {
                //Calculating the new average values
                float dummvarDepth = ActualSamplecount * AvgDepth + Res.Depth;
                float dummvarTemperature = ActualSamplecount * AvgTemperarture + Res.Temperature;
                ActualSamplecount++;
                AvgDepth = dummvarDepth / ActualSamplecount;
                AvgTemperarture = dummvarTemperature / ActualSamplecount;

                //IS it time to write data to datalogfile??
                int logratee = GlobalM.applicationSettingsM.AppSettingsData.DataLogRate;
                int sampleratee = GlobalM.applicationSettingsM.AppSettingsData.SampleRate;
                if (ActualSamplecount >= logratee / sampleratee)
                {
                    ActualSamplecount = 0; AvgDepth = 0; AvgTemperarture = 0; //resetting the parameters for running average calculation

                    theDataloghandler.LogDataset("Dette er headeren", 2.34f, 16.5f, DateTime.UtcNow);
                }

                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }




        /// <summary>
        /// Reading sample from the rs485 sensor
        /// </summary>
        /// <returns></returns>
        public Result GetDataRS485()
        {
            Result res = new Result();

            try
            {
                string comportselected = GlobalM.applicationSettingsM.AppSettingsData.ComportName;

                SerialPort sp = new SerialPort(comportselected);
                sp.Open();

                sp.BaudRate = GlobalM.applicationSettingsM.AppSettingsData.Baudrate;
                sp.Parity = GlobalM.applicationSettingsM.AppSettingsData.parity;
                sp.DataBits = GlobalM.applicationSettingsM.AppSettingsData.Databits;
                sp.StopBits = GlobalM.applicationSettingsM.AppSettingsData.stopbits;
                var t1 = sp.BaudRate;
                var t2 = sp.Parity;
                var t3 = sp.DataBits;
                var t5 = sp.StopBits;
                var t4 = sp.Encoding;

                if (sp.IsOpen)
                {
                    bool end = false;

                    //form the telegram  
                    //1: DeviceNodeNumber  , default 1 but can be set to different if more sensors on the rs485 bus
                    //2: function , 3= read registers
                    //3: Start adress of registers Hi byte   
                    //4: Start adress of registers Low byte   
                    //5: number of registers to read Hi Byte
                    //6: number of registers to read Low Byte
                    //7: CRC result Low
                    //8: CRC result Hi 

                    //byte[] id = new byte[] { 0x01, 0x03, 0x01, 0x90, 0x00, 0x04, 0x45, 0xD8 };

                    byte[] id0 = new byte[] { 0x01, 0x03, 0x01, 0x90, 0x00, 0x04 };
                    UInt16 d1 = ComputeCrc(id0);  // calculating the Checksum
                    byte[] id1 = GetBytesUInt16(d1); //convert to bytes

                    // Make the array larger by 2 elements.
                    int newlenght = id0.Length + 2;
                    Array.Resize(ref id0, newlenght);
                    id0[newlenght - 2] = id1[0];  // adding the 2 bytes for checksum
                    id0[newlenght - 1] = id1[1];


                    byte[] rec = new byte[32];
                    sp.Write(id0, 0, id0.Length);
                    System.Threading.Thread.Sleep(200);  // give the sensor a shor time to respond
                    //var result = sp.ReadExisting();
                    sp.Read(rec, 0, rec.Length);



                    //order the bytes returned by sp.Read so the data is ordered from most significant bit to lowest
                    byte[] rec_ordered = new byte[4];
                    rec_ordered[0] = rec[9];
                    rec_ordered[1] = rec[10];
                    rec_ordered[2] = rec[7];
                    rec_ordered[3] = rec[8];
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(rec_ordered);
                    }
                   res.Depth = BitConverter.ToSingle(rec_ordered, 0);

                    rec_ordered[0] = rec[9];
                   rec_ordered[1] = rec[10];
                   rec_ordered[2] = rec[7];
                   rec_ordered[3] = rec[8];
                   if (BitConverter.IsLittleEndian)
                   {
                       Array.Reverse(rec_ordered);
                   }
                   res.Temperature = BitConverter.ToSingle(rec_ordered, 0);

                }
                else
                {
                }
                sp.Close();

                res.UtcTimestamp = DateTime.UtcNow;

            }
            catch (Exception ex)
            {

                EventLogHandler.WriteEventLog(LogSeverity.SystemError, "GetDataRS485", ex.Message);
                return null;

            }
            return res;
        }


        /// <summary>
        /// Get the CRC data (checksum calculation)
        /// </summary>
        /// <param name="data">The Bytearray for which to get the checksum</param>
        /// <returns></returns>
        private UInt16 ComputeCrc(byte[] data)
        {
            ushort crc = 0xFFFF;

            foreach (byte datum in data)
            {
                crc = (ushort)((crc >> 8) ^ CrcTable[(crc ^ datum) & 0xFF]);
            }

            return crc;
        }//EndOfMethod


        /// <summary>
        /// Convert a ushort argument to a byte array and display it. 
        /// </summary>
        /// <param name="argument"></param>
        private byte[] GetBytesUInt16(ushort argument)
        {
            byte[] byteArray = BitConverter.GetBytes(argument);
            var res = BitConverter.ToString(byteArray);
            return byteArray;
        }


        //private void ResultWriter(Result res) { }
        //private void EventWriter(string EventStr) { }
        //private void AlarmWriter(string AlarmStr) { }






    }
}
