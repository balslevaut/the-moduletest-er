﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;



namespace MHydrostaticLibrary1
{

    static public class GlobalM
    {

        static public ApplicationSettingsM applicationSettingsM = new ApplicationSettingsM (); // Settings from the settingsfile is made global 
    }

    public class MMainRouitine
    {
        bool Dosampledone= false ; // flag used to tell if the sampling sequence has finished 
        private System.ComponentModel.BackgroundWorker backgroundWorkerM1;
        



        public RS485DataCom SensorHandleObject;
        private int State = 0;
        private int reloadSetting = 0;


        void Initialize()
        {
            backgroundWorkerM1 = new BackgroundWorker();

            backgroundWorkerM1.DoWork += new DoWorkEventHandler(backgroundWorkerM1_DoWork);
            backgroundWorkerM1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerM1_RunWorkerCompleted);
            backgroundWorkerM1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorkerM1_ProgressChanged);
            SensorHandleObject = new RS485DataCom();
            State = 1;

            ReadSettingsfile();  // read the settings from xml settings file


        }

        /// <summary>
        /// Read settingsfile wrapped and able to recall if necessary
        /// </summary>
        private void ReadSettingsfile()
        {
            GlobalM.applicationSettingsM.ReadAllsettings();  // read the settings from xml settings file
        }


        public void MMainRouitineRT()
        {
            // main routine that takes care of the housekeeping of the service 
            // Runnning infinite loop , and restating id something goes completely wrong
            while (true)
            {


                try
                {
                    //Initialize first if needed do a re-initialize
                    if (State == 0)
                    {
                        Initialize();
                    }

                    if (backgroundWorkerM1.IsBusy != true)
                    {
                        // Start the asynchronous operation.
                        backgroundWorkerM1.RunWorkerAsync();
                    }
                    int MilliSecondstoSleep = GlobalM.applicationSettingsM.AppSettingsData.DataLogRate * 1000;
                    if (GlobalM.applicationSettingsM.AppSettingsData.UseSubSampling) MilliSecondstoSleep = GlobalM.applicationSettingsM.AppSettingsData.SampleRate * 1000;

                    Thread.Sleep(MilliSecondstoSleep);
                    if (backgroundWorkerM1.IsBusy || Dosampledone==false)
                    {
                        backgroundWorkerM1.CancelAsync();

                        EventLogHandler.WriteEventLog(LogSeverity.Alarm, "Backgroundworkerthread", "Timeout for the datasampling thread");
                        //alarm here, communication failed
                    }

                }
                catch (Exception)
                {
                    EventLogHandler.WriteEventLog(LogSeverity.SystemError, "MainLoop exeption", "Exeption in the main executionloop");
                }
            }

        }

        private void backgroundWorkerM1_DoWork(object sender, DoWorkEventArgs e)
        {
            reloadSetting++;
            if (reloadSetting >= 10)
            {
                ReadSettingsfile(); 
                reloadSetting = 0;
            }
            //do the sampling and write the datalog if its time for writing
            Dosampledone= SensorHandleObject.DoSampleData();

        }

        // This event handler deals with the results of the
        // background operation.
        private void backgroundWorkerM1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        // This event handler updates the progress bar.
        private void backgroundWorkerM1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }






    }
}
