﻿using MCommonLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MIclServerLibrary
{
    public class ICLsHandler
    {


        List<Thread> workerThreads;

        string className = "ICLsHandler";

        public void Stop()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "in Stop");
            foreach (Thread th in workerThreads)
            {
                th.Abort();            
            }
        }

        public void UpdateAndReadIcls()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "in UpdateAndReadIcls");
            string ports = ConfigFileWrapper.ReadSetting("Port");
            workerThreads = new List<Thread>();
            
            if (ports != null)
            { 
                string[] portsArray = ports.Split(',');

                foreach (string port in portsArray)
                {                    
                    Icl icl =  new Icl(port);
                    Thread workerThread = new Thread(icl.StartMe);
                    workerThread.Start();
                    workerThreads.Add(workerThread);
                }

                foreach (var thread in workerThreads)
                {
                    thread.Join();
                }
                
                

                /*
                Icl icl = new Icl(ports);

                Thread workerThread = new Thread(icl.StartMe);
                workerThread.Start();

                workerThread.Join();
                */
                EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "In UpdateAndReadIcls Done!!");
                EventLogHandler.flushEventLog(); // We want to ensure everything is flushed

                // Now where all data has been transported we change the library where data and logs are stored
                EventLogHandler.SetFilePath();
                // Change the directory

            }
        }

        ~ICLsHandler()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "in  ~ICLsHandler()");
        }
    }
}
