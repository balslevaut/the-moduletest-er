﻿using MCommonLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace MIclServerLibrary
{

    public delegate void MyTimerEventDelegate(MyServerEvents myServerEvents);

    // Tells every minute if activity is ongoing or not. Can be used to supervise whether something maybe is blocked ('Wathdog feature').
    public delegate void MyOneMinuteTimerDelegate(DateTime dt, bool activityOngoing);

    public enum MyServerEvents
    {
        None,                           // No activity, right now
        TurnOnIcls,                     // Time for waking up ICL's
        TurnOffIcls,                    // Time for turning of ICL's
        AskForLoggerDataTurnOnIcls,     // Ask for logger data, but first turn on ICL's so measurements can be done
        AskForLoggerDataNow,            // Ask for logger data now. When this session is done       
    }


    // This class keeps an eye on all the activities that are ongoing.
    public class TimeEventsSupervisor
    {
        static string className = "TimeEventsSupervisor";

        // 1 second timer
        System.Timers.Timer myTimer = new System.Timers.Timer();

        static MyTimerEventDelegate myTimerEventDelegate;
        static MyOneMinuteTimerDelegate myOneMinuteTimerDelegate;
        static int cLockForReading;          // The time on the day where data is fetch (In hour)
        static int frequencyForStartingICL;  // How often is the ICL going to be turned on?
        static int minutesPowerIsTurnedOn;   // How many minutes is the ICL turned on so they can make a measurement?
        private static bool activityOngoing = false;

        // Is used to protect the "activityOngoing" parameter
        private static Mutex mut = new Mutex();       
        
        static MyServerEvents nextEvent = MyServerEvents.None;
        static DateTime timeForNextEvent = DateTime.MinValue;

        public TimeEventsSupervisor(MyTimerEventDelegate timerEventDelegate, MyOneMinuteTimerDelegate oneMinuteTimerDelegate)
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "TimeEventsSupervisor constructor called");
            myTimerEventDelegate = timerEventDelegate;
            myOneMinuteTimerDelegate = oneMinuteTimerDelegate;
        }

        public void Start()
        {
            try
            {
                cLockForReading = Int32.Parse(ConfigFileWrapper.ReadSetting("CLockForReading"));
                frequencyForStartingICL = Int32.Parse(ConfigFileWrapper.ReadSetting("FrequencyForStartingICL"));
                minutesPowerIsTurnedOn = Int32.Parse(ConfigFileWrapper.ReadSetting("MinutesPowerIsTurnedOn"));

            }
            catch (Exception ee)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "Could not read configuration data. So Service NOT started:" + ee);
                return;
            }

            try
            {
                myTimer.Elapsed += new ElapsedEventHandler(OneSecondTimerEventHandler);
                myTimer.Interval = 1000; // 1000 ms is one second  

                myTimer.Start();
            }
            catch (Exception ee)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "Could not start 1 second timer. So Service NOT started:" + ee);
            }
        }

        public void Stop()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Info, className, "Stop 1 second timer");

            try
            {
                if (myTimer != null)
                {

                    myTimer.Stop();
                    myTimer.Dispose();
                    myTimer = null;
                }
            }
            catch { }

        }

        public bool IsMyTimerRunning()
        {
            if (myTimer == null)
            {
                return false;
            }
            else
            {
                return myTimer.Enabled;
            }

        }

        private static void CalcTimeForNextEventAndSetState()
        {
            DateTime dt = DateTime.UtcNow;
            TimeSpan ts = new TimeSpan(dt.Hour, (dt.Minute / frequencyForStartingICL + 1) * frequencyForStartingICL, 0);
            timeForNextEvent = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0) + ts;

            if (timeForNextEvent.Hour == cLockForReading && timeForNextEvent.Minute == 0)
            {
                nextEvent = MyServerEvents.AskForLoggerDataTurnOnIcls;
            }
            else
            {
                nextEvent = MyServerEvents.TurnOnIcls;
            }
        }

        public static void OneSecondTimerEventHandler(object source, ElapsedEventArgs e)
        {
            // Code here will run every second

            DateTime dtNow = DateTime.UtcNow;

            if (dtNow.Second == 0)
            {
                // Inform whether activity is ongoing or not
                myOneMinuteTimerDelegate(dtNow, activityOngoing);
            }

            if (activityOngoing == false)
            {
                mut.WaitOne();
                activityOngoing = true;
                mut.ReleaseMutex();

                try
                {                    
                    if (dtNow.Second == 0)
                    {
                        EventLogHandler.WriteEventLog(LogSeverity.Info, className, "TimeForNextEvent (1 minut), nextEvent =" + nextEvent);
                    }

                    if (timeForNextEvent == DateTime.MinValue)
                    {
                        // Server just been started calculate time. For next event

                        if (60 % frequencyForStartingICL != 0)
                        {
                            EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "Configuration ERROR: FrequencyForStartingICL is not a fraction of 60");
                        }

                        CalcTimeForNextEventAndSetState();
                    }

                    if (timeForNextEvent <= dtNow)
                    {
                       // if (dtNow.Second == 0)
                       // {
                            EventLogHandler.WriteEventLog(LogSeverity.Info, className, "timeForNextEvent passed, nextEvent =" + nextEvent);
                       // }

                         //   nextEvent = MyServerEvents.AskForLoggerDataNow;  // JENS SKAL FJERNES IGEN
                         //   EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "JENS SKAL FJERNES IGEN: nextEvent = MyServerEvents.AskForLoggerDataNow;");
                        

                        // Inform about event
                        myTimerEventDelegate(nextEvent);

                        // New event time               
                        switch (nextEvent)
                        {
                            case MyServerEvents.AskForLoggerDataNow:                                
                                CalcTimeForNextEventAndSetState();
                                break;

                            case MyServerEvents.AskForLoggerDataTurnOnIcls:

                                timeForNextEvent += new TimeSpan(0, minutesPowerIsTurnedOn, 0);   // We add defined minutes where the ICL is turned on so measurments can be done
                                nextEvent = MyServerEvents.AskForLoggerDataNow;
                                break;

                            case MyServerEvents.None:
                                // Should not happended
                                break;

                            case MyServerEvents.TurnOffIcls:
                                CalcTimeForNextEventAndSetState();
                                break;

                            case MyServerEvents.TurnOnIcls:
                                timeForNextEvent += new TimeSpan(0, minutesPowerIsTurnedOn, 0);
                                nextEvent = MyServerEvents.TurnOffIcls;
                                break;
                        }
                        EventLogHandler.WriteEventLog(LogSeverity.Info, className, "Next time event defined: nextEvent =" + nextEvent + ", Time = " + timeForNextEvent);
                    }
                }
                catch (Exception ee)
                {
                    EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "Severe error: " + ee);
                }
                finally
                {
                    if (mut != null)
                    {
                        mut.WaitOne();
                        activityOngoing = false;
                        mut.ReleaseMutex();
                    }
                }
            }
        }

        ~TimeEventsSupervisor()
        {
            Stop();

            if (mut != null)
            {
                mut.Dispose();
                mut = null;
            }
        }
    }
}
