﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.BDaq;
using MCommonLibrary;
using System.Globalization;

namespace MIclServerLibrary
{
    public class RelayHandler
    {
        InstantDoCtrl instantDoCtrl = null;

        string deviceDescription = "PCI-1761,BID#0";

        static string className = "RelayHandler";

        static bool BioFailed(ErrorCode err)
        {
            return err < ErrorCode.Success && err >= ErrorCode.ErrorHandleNotValid;
        }

        public RelayHandler()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "RelayHandler constructor");
        }

        public RelayHandler(bool t)
        {
            // Dummy relay handler where the eventloghandler is not called;
        }


        public void ActivateRelays(byte[] myBufferForWriting, bool? doNotLog = null)
        {
            if (doNotLog == null)
            {
                EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "ActivateRelays");
            }
            ErrorCode errorCode = ErrorCode.Success;
            try
            {
                instantDoCtrl = new InstantDoCtrl();
                instantDoCtrl.SelectedDevice = new DeviceInformation(deviceDescription);

                errorCode = instantDoCtrl.Write(0, 1, myBufferForWriting);
                if (BioFailed(errorCode))
                {
                    throw new Exception();
                }
            }
            catch (Exception ee)
            {
                if (doNotLog == null)
                {
                    string errStr = BioFailed(errorCode) ? " Some error occurred. And the last error code is " + errorCode.ToString()
                                                             : ee.Message;
                    EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, errStr);
                }
            }
        }

        public void ActivateAllRelays()
        {
            byte[] bufferForWriting = new byte[64];
            byte x = 15;
            bufferForWriting[0] = x;
            ActivateRelays(bufferForWriting,true);
        }

        public void ActivateRelays()
        {
            byte[] bufferForWriting = new byte[64];

            byte x = 0;
            try
            {

                string enabledKey = ConfigFileWrapper.ReadSetting("EnabledRelays");

                x = Byte.Parse(enabledKey);
            }
            catch (Exception ee)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "Not possible to read config parameter: 'EnabledRelays' and it is not a hexadecimal kode. Exception: " + ee.ToString());
            }

            bufferForWriting[0] = x; // Hårdkodet lige nu, skal være configurerbar
            ActivateRelays(bufferForWriting);
        }

        public void DeactivateRelays(bool? doNotLog = null)
        {
            // Step 4: Close device and release any allocated resources.
            if (doNotLog == null)
            {
                EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "DeactivateRelays");
            }

            if (instantDoCtrl != null)
            {
                byte[] bufferForWriting = new byte[64];
                bufferForWriting[0] = 0x0;
                ActivateRelays(bufferForWriting, doNotLog); // Deactivate activated Relays
                instantDoCtrl.Dispose();
                instantDoCtrl = null;
            }
        }
    }
}
