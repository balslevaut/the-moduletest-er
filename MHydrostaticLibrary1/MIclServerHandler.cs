﻿using MCommonLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MIclServerLibrary
{

    // This class is written to make it more easy to test MIclserver. (This class can be used by both MICLserver and TestIclMserver)

    public class MIclServerHandler
    {
        static System.Diagnostics.EventLog eventLog = null;

        // The timer that is called the very first time. So the service activity is a little bit delay and not times out when started
        private System.Threading.Timer intervalTimer;

        // Remember if activity is ongoing right now. Is used by the watchdog to ensure the system is not logged
        bool isActivityOngoing = false;  // Jens: Default changed to 'false'
        private Mutex mut = new Mutex();   // Is used to protect the tString


        TimeEventsSupervisor timeEventsSupervisor = null;

        Watchdog myWatchdogObject = null;

        ICLsHandler iCLsHandler = null;

        RelayHandler relayHandler = null;

        static string className = "MIclServerHandler";
        private ManualResetEvent mre = null;  

        ~MIclServerHandler()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className , "~MIclServerHandler()");
        }

        // Called when the service started. Might also be called in case a restart of the service is needed
        public void OnStart(System.Diagnostics.EventLog myEventLog)
        {
            try
            {
                eventLog = myEventLog;
                EventLogHandler.SetEventLog(myEventLog);
                EventLogHandler.WriteEventLog(LogSeverity.Info, className, "In OnStart");

                // Start the first read 10 seconds from now
                int timerTime = 10;
                TimeSpan tsInterval = new TimeSpan(0, 0, timerTime);

                if (intervalTimer != null)
                {
                    intervalTimer.Dispose();
                    intervalTimer = null;
                }

                intervalTimer = new System.Threading.Timer(new
                    TimerCallback(MyTimer_elapsed), null, (int)tsInterval.TotalMilliseconds,
                    (int)tsInterval.TotalMilliseconds);
            }
            catch (Exception ee)
            {
                // The service has failed. Very serious error 
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "The service has failed. Exception = " + ee);
            }
        }


        private void ResetSystem()
        {          
            try
            {
                if (mre != null)
                {
                     // Kill the current onStart
                mre.Set();
                mre  = null;              
                }

                if (intervalTimer != null)
                {
                    intervalTimer.Dispose();
                    intervalTimer = null;
                }
                isActivityOngoing = true;
                timeEventsSupervisor.Stop();
                timeEventsSupervisor = null;

                if (iCLsHandler != null)
                {
                    iCLsHandler.Stop();
                    iCLsHandler = null;
                }

                if (intervalTimer != null)
                {
                    intervalTimer.Dispose();
                    intervalTimer = null;
                }

                if (myWatchdogObject != null)
                {
                    myWatchdogObject.Stop();
                    myWatchdogObject = null;
                }

                if (relayHandler != null)
                {
                    relayHandler.DeactivateRelays();
                }

                // Start again, and this time we hope no problems occurs
                OnStart(eventLog);
            }
            catch (Exception ee)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "Reset System failed: " + ee);
            }
        }

        public void MyOneMinuteTimerDelegateMethod(DateTime dt, bool activityOngoing)
        {
            EventLogHandler.WriteEventLog(LogSeverity.Info, className, "MyOneMinuteTimerDelegateMethod called, activityOngoing= " + activityOngoing);
            isActivityOngoing = activityOngoing;

        }

     //  Boolean SkalSLETTES_IGEN = true;
        public void MyTimerEventDelegateMethod(MyServerEvents myServerEvents)
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "In MyTimerEventDelegateMethod called in state:" + myServerEvents);
       /*     
            if (SkalSLETTES_IGEN == true)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, "SKAL SLETTES IGEN", "SKAL SLETTES IGEN");
                myServerEvents = MyServerEvents.AskForLoggerDataNow; // Jens, skal fjernes igen.........................
                SkalSLETTES_IGEN = false;
            }
         */
            switch (myServerEvents)
            {
                case MyServerEvents.AskForLoggerDataNow:

                    iCLsHandler = new MIclServerLibrary.ICLsHandler();

                    // The below call does not return before communication with all ICL's are done.
                    iCLsHandler.UpdateAndReadIcls();                   
                    relayHandler.DeactivateRelays(); // Jens, added 12 september 2016 to avoid the ICLs are turned on
                    EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "iCLsHandler.UpdateAndReadIcls() passed:");
                    break;

                case MyServerEvents.AskForLoggerDataTurnOnIcls:
                    relayHandler.ActivateRelays();
                    break;

                case MyServerEvents.TurnOffIcls:
                    relayHandler.DeactivateRelays();
                    break;

                case MyServerEvents.TurnOnIcls:
                    relayHandler.ActivateRelays();
                    break;

                default:
                    break;
            }
        }


        public bool MyWatchdogTimerDelegate()
        {
            return isActivityOngoing;
        }

        public void OnStop(System.Diagnostics.EventLog myEventLog)
        {
            EventLogHandler.WriteEventLog(LogSeverity.Info, className, "In OnStop");
            if (timeEventsSupervisor != null)
            {
                timeEventsSupervisor.Stop();
            }

            EventLogHandler.flushEventLog();

            // Allow the event to die
            if (mre != null)
            {                
                mre.Set();
                mre = null;
            }
        }

        // There has been some cases where the one minute timer stops running. The reason is unknown but we have to keep an eye on that it is running
        public bool IsOneMinuteTimerRunning()
        {
            if (timeEventsSupervisor == null)
            {
                return false;
            }
            else
            {
                return timeEventsSupervisor.IsMyTimerRunning();                
            }


        }

        private void MyTimer_elapsed(object state)
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "In MyTimer_elapsed");

            try
            {

                // Stop the interval timer
                intervalTimer.Dispose();
                intervalTimer = null;

                // Start wathdog timer

                if (myWatchdogObject != null)
                {
                    myWatchdogObject.Stop();
                    myWatchdogObject = null;
                }

                myWatchdogObject = new Watchdog();
                myWatchdogObject.Start(MyWatchdogTimerDelegate, ResetSystem, IsOneMinuteTimerRunning);

                relayHandler = new RelayHandler();

                // Ensure the relays are disabled in case they were enabled when the server died and now is restarted.
                byte[] bufferForWriting = new byte[64];
                bufferForWriting[0] = 0x0;
                relayHandler.ActivateRelays(bufferForWriting);


                // Hvis du lige vil teste at data kan gemmes. Skal FJERNES igen.
                // ICLsHandler iCLsHandler = new MIclServerLibrary.ICLsHandler();
                // iCLsHandler.UpdateAndReadIcls();
                // return;          
    
                if (timeEventsSupervisor != null)
                {
                    timeEventsSupervisor.Stop();
                    timeEventsSupervisor = null;
                }

                timeEventsSupervisor = new TimeEventsSupervisor(MyTimerEventDelegateMethod, MyOneMinuteTimerDelegateMethod);
                timeEventsSupervisor.Start();

                if (mre != null)
                {
                    mre.Set();
                    mre = null;
                }

                mre  = new ManualResetEvent(false);
                EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "In MyTimer_elapsed;  mre.WaitOne() called;");
                mre.WaitOne();
                EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "In MyTimer_elapsed;  mre.WaitOne() passed;");

            }
            catch (Exception eee)
            {
                string str1 = "Serious Errror: MyTimer_Elapsed: " + eee.ToString();
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, str1);
            }
        }

        private void MServerEventlog(string msg)
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, msg);
        }
    }
}
