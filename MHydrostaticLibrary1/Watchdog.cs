﻿using MCommonLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MIclServerLibrary
{
    // This object shall run independently from everything else so we are sure it is not blocked and can take action if needed
    // Ask every minute if activity is ongoing or not. Can be used to supervise whether something maybe is blocked ('Wathdog feature').
    class Watchdog
    {
       
        public delegate bool WatchdogTimerDelegate();   // Delegate for Returning whether activity is ongoing or not
        public delegate void ResetDelegate();           // Delegate that can be called by the watchdog in case a system reset is needed
        public delegate bool IsOneMinuteTimerRunningDelegate(); // Delegate that keeps an eye on whether the one minute timer is running 


        System.Timers.Timer myWatchdogTimer = new System.Timers.Timer();

        static DateTime lastTimeActivityWasOff = DateTime.UtcNow;  // Last time no activity was ongoing.

        static string className = "Watchdog";

        static WatchdogTimerDelegate myStaticWatchdogTimerDelegate;
        static ResetDelegate myStaticResetDelegate;
        static IsOneMinuteTimerRunningDelegate myIsOneMinuteTimerRunningDelegate;

        public void Stop()
        {
            if (myWatchdogTimer != null)
            {
                myWatchdogTimer.Stop();
                myWatchdogTimer.Dispose();
            }

            
        }


        public void Start(WatchdogTimerDelegate myWatchdogTimerDelegate, ResetDelegate myResetDelegate, IsOneMinuteTimerRunningDelegate isOneMinuteTimerRunningDelegate)
        {
            myStaticWatchdogTimerDelegate = myWatchdogTimerDelegate;

            myStaticResetDelegate = myResetDelegate;

            myIsOneMinuteTimerRunningDelegate = isOneMinuteTimerRunningDelegate;

            myWatchdogTimer.Elapsed += new ElapsedEventHandler(OneMinuteTimerEventHandler);
            myWatchdogTimer.Interval = 1000 * 60; // 1000 ms is one second, so one minute  

            myWatchdogTimer.Start();
        }

        public static void OneMinuteTimerEventHandler(object source, ElapsedEventArgs e)
        {
            // Code here will run every minute
           
            // Ask whether activity is ongoing
            bool result = myStaticWatchdogTimerDelegate();

            bool result2 = myIsOneMinuteTimerRunningDelegate();

            if (result == false)
            {
                lastTimeActivityWasOff = DateTime.UtcNow;
            }

            EventLogHandler.WriteEventLog(LogSeverity.Info, className, "Wathdog called, activityOngoing= " + result +
               ", lastTimeActivityWasOff: " + lastTimeActivityWasOff + ", OneMinuteTimerRunningDelegate= " + result2);

            int watchdogPeriod = Int32.Parse(ConfigFileWrapper.ReadSetting("WathDogTime"));

            if (lastTimeActivityWasOff + TimeSpan.FromMinutes(watchdogPeriod) < DateTime.UtcNow)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "WatchDog detected. Reset system:");
                myStaticResetDelegate();
            }
            
            if (result2 == false)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, className, "One minute timer is not running. Reset system:");
                myStaticResetDelegate();
            }
        }

        ~Watchdog()
        {
            EventLogHandler.WriteEventLog(LogSeverity.Debug, className, "in  ~Watchdog()");
            Stop();
        }
    }
}
