﻿using MCommonLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIclServerLibrary
{
    public class ConfigFileWrapper
    {
        public static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                return appSettings[key];                
            }
            catch (ConfigurationErrorsException)
            {
                EventLogHandler.WriteEventLog(LogSeverity.SystemError, "ConfigFileWrapper" , "Error, cannot read: " + key);
                return null;                
            }
        }
    }
}
