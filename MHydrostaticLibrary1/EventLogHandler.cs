﻿//using MCommonLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MHydrostaticLibrary1
{
    public enum LOGLEVEL { SEVERE, INFO, DEBUG };

    public class EventLogHandler
    {
      
        static System.Diagnostics.EventLog myEventLog = null;
        static StringBuilder sb = null;

        // Prevent that next reading session is started before the previous has completed
        private static Mutex mut = new Mutex();

        public static void SetEventLog(System.Diagnostics.EventLog eventLog)
        {
            mut.WaitOne();
            myEventLog = eventLog;
            sb = new StringBuilder();
            mut.ReleaseMutex();
        }

        public static void WriteEventLog(LogSeverity level, string source, string txt)
        {
            mut.WaitOne();
            StringBuilder sb = new StringBuilder();
            try
            {
                string mytxt = DateTime.UtcNow.ToString("yyyy_MMM_dd HH:mm:ss") + ": " + level.ToString() + ": '" + source + "', '" + txt + "'";
                Console.WriteLine(mytxt);
                sb.AppendLine(mytxt);
                //if (sb.Length > 512)
                if (sb.Length > 2)
                {
                    try
                    {
                        // Write data to file:
                        File.AppendAllText(GetEventLogFileNameAndPath(), sb.ToString());
                        sb.Clear();
                    }
                    catch
                    { }

                }
                if (level == LogSeverity.SystemError)
                {
                    File.AppendAllText(GetSevereLogFileNameAndPath(), mytxt + "\r\n");
                }
            }
            catch (Exception ee)
            {
                myEventLog.WriteEntry("WriteEventLog failed: " + ee);
            }
            finally
            {
                mut.ReleaseMutex();
            }
        }

        public static void flushEventLog()
        {
            mut.WaitOne();
            try
            {
                if (sb.Length > 0)
                {
                    // Write data to file:
                    File.AppendAllText(GetEventLogFileNameAndPath(), sb.ToString());
                    sb.Clear();
                }
            }
            catch (Exception ee)
            {
                myEventLog.WriteEntry("flushEventLog failed: " + ee);

            }
            finally
            {
                mut.ReleaseMutex();
            }
        }


        // Can be used by different common classes with WEB service to deliver log data
        public static void MyCallbackLog(DateTime timeStamp, LogSeverity severity, string customerName, string observation)
        {            
            WriteEventLog(severity, customerName,observation);
        }


        static string currentFilePath = null;
        public static void SetFilePath()
        {
            mut.WaitOne();
            try
            {
                DateTime dt = DateTime.UtcNow;

                // Acd files have been stored and a new 24 hour event is going to happended
                int hourForReading = GlobalM.applicationSettingsM.AppSettingsData.HourForReading;

                currentFilePath =GlobalM.applicationSettingsM.AppSettingsData.AppFolderPath;

                if (dt.Hour < hourForReading)
                {
                    // We use the data from previous day
                    currentFilePath += "\\" + DateTime.UtcNow.AddHours(-24).ToString("yyyy_MMM_dd") + "_" + hourForReading.ToString("00") + "00\\";
                }
                else
                {
                    currentFilePath += "\\" + DateTime.UtcNow.ToString("yyyy_MMM_dd") + "_" + hourForReading.ToString("00") + "00\\";
                }

                // Check if libray is missing
                try
                {
                    // create a new folder.
                    Directory.CreateDirectory(currentFilePath);
                }
                catch { }

                //We also clean the fileEvent and errorLogName to ensure that those paths and updated also
                fileEventName = null;
                errorLogName = null;
            }
            catch { }

            finally
            {
                mut.ReleaseMutex();
            }
        }

        public static string GetFilePath()
        {

            return GlobalM.applicationSettingsM.AppSettingsData.DataFolderPath +@"\";
        }

        static string fileEventName = null;
        public static string GetEventLogFileNameAndPath()
        {
            if (fileEventName == null)
            {
                int hourForReading = GlobalM.applicationSettingsM.AppSettingsData.HourForReading;
                DateTime dt = DateTime.UtcNow;

                if (dt.Hour < hourForReading)
                {
                    // We use the data from previous day                  
                    fileEventName = GetFilePath() + "Event_Log_" + DateTime.UtcNow.AddHours(-24).ToString("yyyy_MMM_dd") + ".log";
                }
                else
                {
                    fileEventName = GetFilePath() + "Event_Log_" + DateTime.UtcNow.ToString("yyyy_MMM_dd") + ".log";
                }
            }
            return fileEventName;
        }

        static string errorLogName = null;
        public static string GetSevereLogFileNameAndPath()
        {
            if (errorLogName == null)
            {

                int hourForReading = GlobalM.applicationSettingsM.AppSettingsData.HourForReading;
                DateTime dt = DateTime.UtcNow;

                if (dt.Hour < hourForReading)
                {
                    errorLogName = GetFilePath() + "ERROR_Log_" + DateTime.UtcNow.AddHours(-24).ToString("yyyy_MMM_dd") + ".log";
                }
                else
                {
                    errorLogName = GetFilePath() + "ERROR_Log_" + DateTime.UtcNow.ToString("yyyy_MMM_dd") + ".log";
                }
            }
            return errorLogName;
        }

   
    }
}
